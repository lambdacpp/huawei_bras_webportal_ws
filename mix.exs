defmodule HWBrasWebPortalWS.Mixfile do
  use Mix.Project

  def project do
    [app: :huawei_bras_webportal_ws,
     version: "0.2.0",
     elixir: "~> 1.2",
     erlc_paths: ["src"],
     build_embedded: Mix.env == :prod,
     start_permanent: Mix.env == :prod,
     deps: deps]
  end

  # Configuration for the OTP application
  #
  # Type "mix help compile.app" for more information
  def application do
    [applications: [:logger, :maru, :logger_file_backend],
     mod: {HWBrasWebPortalWS,[]}
    ]
  end

  # Dependencies can be Hex packages:
  #
  #   {:mydep, "~> 0.3.0"}
  #
  # Or git/path repositories:
  #
  #   {:mydep, git: "https://github.com/elixir-lang/mydep.git", tag: "0.1.0"}
  #
  # Type "mix help deps" for more examples and options
  defp deps do
    [{:maru, "~> 0.10"},
     {:cors_plug, "~> 1.1"},
     {:logger_file_backend, "~> 0.0.9"}]
  end
end
