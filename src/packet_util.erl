-module(packet_util).

-export([make_packet/7,unmashalling/1]).

value_to_tlvbin(Type,Value) ->
    ValueBin = list_to_binary(Value),
    Len = byte_size(ValueBin)+2,
    <<Type:8,Len:8,ValueBin/binary>> .

make_packet(Type,SerialNo,chap,ReqId,UserIp,AttrList,Secret) ->
    make_packet(Type,SerialNo,0,ReqId,UserIp,AttrList,Secret);
make_packet(Type,SerialNo,pap,ReqId,UserIp,AttrList,Secret) ->
    make_packet(Type,SerialNo,1,ReqId,UserIp,AttrList,Secret);
make_packet(Type,SerialNo,CP,ReqId,UserIp,AttrList,Secret) ->
    {Ip1,Ip2,Ip3,Ip4} = UserIp,
    AttrNum = length(AttrList),
    Header = <<2:8,       %% Version : 2  
               Type:8,
               CP:8,     %% Chap/0:PAP/1
               0:8,       %% Reserved
               SerialNo:16,
               ReqId:16,      
               Ip1:8,
               Ip2:8,
               Ip3:8,
               Ip4:8,
               0:16,      %% UserPort/0
               0:8,       %% ErrCode
               AttrNum:8>>,     
    AttrBin = lists:foldl(fun(Bin,SumBin) -> <<SumBin/binary,Bin/binary>> end, <<>>,
                         lists:map(fun(E) -> {T,V}=E, value_to_tlvbin(T,V)  end, AttrList)),
    
    Hash = erlang:md5([Header,<<0:128>>,AttrBin,Secret]),
    <<Header/binary,Hash/binary,AttrBin/binary >> .



unmashalling(Dgram) ->
    case Dgram of
        <<2:8,       %% Version : 2  
          Type:8,
          _:8,     %% Chap/0:PAP/1
          0:8,       %% Reserved
          SerialNo:16,
          ReqId:16,      
          Ip1:8,
          Ip2:8,
          Ip3:8,
          Ip4:8,
          _:16,      %% UserPort/0
          ErrCode:8,
          _:8, %% AttrNum
          _:128,
          AttrBin/binary  >>  ->
            {Type,SerialNo,ReqId,{Ip1,Ip2,Ip3,Ip4},ErrCode,unmashalling_attrs_bin(AttrBin)};
        _ ->
            bypass
    end.


unmashalling_attrs_bin(AttrBin) ->
    unmashalling_attrslist(binary_to_list(AttrBin),[]).

unmashalling_attrslist(Attr,AttrList) ->
    case Attr of 
        [Type,Len|Tail] when length(Tail) >= (Len-2) ->
            {V,R} = lists:split(Len-2,Tail),
            unmashalling_attrslist(R, AttrList++[{Type,V}]);%%  
        _ ->
            AttrList
    end.
