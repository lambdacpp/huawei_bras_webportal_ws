%%%-------------------------------------------------------------------
%%% @author lambdacpp
%%% @doc 华为Web Portal认证接口通信
%%% @end
%%%-----------------
-module(bras_comm).
-behaviour(gen_server).

%% API
-export([
         start_link/3,
         start_link/2,
         auth/3,
         logout/1,
         info/1,
         stop/0
         ]).

%% gen_server callbacks
-export([init/1, handle_call/3, handle_cast/2, handle_info/2,
         terminate/2, code_change/3]).

-define(SERVER, ?MODULE).      
-define(DEFAULT_PORT, 50100).   
-define(ME60_PORT, 2000).   
-define(DEFAULT_TIMEOUT, 2000).  

-define(REQ_CHALLENGE, 1).
-define(ACK_CHALLENGE, 2).
-define(REQ_AUTH     , 3).
-define(ACK_AUTH     , 4).
-define(REQ_LOGOUT   , 5).
-define(ACK_LOGOUT   , 6).
-define(AFF_ACK_AUTH , 7).
-define(NTF_LOGOUT   , 8).
-define(REQ_INFO     , 9).
-define(ACK_INFO     , 10).


-record(state, {bras_ip ,secret, usock, sessions}). 
%%%===================================================================
%%% API 
%%%===================================================================

%%--------------------------------------------------------------------
%% @doc Starts the server.
%%
%% @spec start_link(Port::integer()) -> {ok, Pid}
%% where
%%  Pid = pid()
%% @end
%%--------------------------------------------------------------------
start_link(BrasIp,Port,Secret) ->
    gen_server:start_link({local, ?SERVER}, ?MODULE, [BrasIp,Port,Secret], []).     %派生服务器进程

%% @spec start_link() -> {ok, Pid}
%% @doc Calls `start_link(Port)' using the default port.
start_link(BrasIp,Secret) ->
    start_link(BrasIp,?DEFAULT_PORT,Secret).

%%--------------------------------------------------------------------
%% @doc Stops the server.
%% @spec stop() -> ok
%% @end
%%--------------------------------------------------------------------
stop() ->
    gen_server:cast(?SERVER, stop).      

%%--------------------------------------------------------------------
%% @doc Send Auth packet and wait replay.
%% @spec auth -> ok
%% @end
%%--------------------------------------------------------------------
auth(UserIp,UserName,Password) ->
    SerialNo = gen_server:call(?SERVER, getsn) ,

    gen_server:cast(?SERVER, {auth, [{userip, UserIp},
                                     {username, UserName},
                                     {password , Password},
                                     {serialno, SerialNo}]}),
    case waiting_sess_state_change(SerialNo, login, req, ?DEFAULT_TIMEOUT, 0, 10) of
        {NewState, SessAttrs} ->
            case NewState of 
                ack ->
                    ReqId = maps:get(reqid,SessAttrs),
                    gen_server:cast(?SERVER, {aff, [{userip, UserIp},{serialno, SerialNo},{reqid, ReqId}]}),
                    ok ;
                error ->
                    ErrCode = maps:get(errid,SessAttrs),
                    gen_server:cast(?SERVER, {remove , [{serialno, SerialNo}]}),
                    {error,ErrCode}
            end;
        timeout ->
            gen_server:cast(?SERVER, {remove , [{serialno, SerialNo}]}),
            {error,timeout} ;
        notfound ->
            %lager:warning("Auth Session [~p] is not in session table ~n",[SerialNo]),
            {error,f_error};
        _ ->
            %lager:warning("Auth Session [~p] Type Error ~n",[SerialNo]),
            {error,f_error}
    end.

logout(UserIp) ->
    SerialNo = gen_server:call(?SERVER, getsn) ,
    gen_server:cast(?SERVER, {logout, [{userip, UserIp},{serialno, SerialNo}]}),
    case waiting_sess_state_change(SerialNo, logout, req, ?DEFAULT_TIMEOUT, 0, 10) of
        {NewState, SessAttrs} ->
            case NewState of 
                ack ->
                    gen_server:cast(?SERVER, {remove , [{serialno, SerialNo}]}),
                    ok ;
                error ->
                    ErrCode = maps:get(errid,SessAttrs),
                    gen_server:cast(?SERVER, {remove , [{serialno, SerialNo}]}),
                    {error,ErrCode}
            end;
        timeout ->
            gen_server:cast(?SERVER, {remove , [{serialno, SerialNo}]}),
            {error,timeout} ;
        notfound ->
            %lager:warning("Logout Session [~p] is not in session table ~n",[SerialNo]),
            {error,f_error};
        _ ->
            %lager:warning("Logout Session [~p] Type Error ~n",[SerialNo]),
            {error,f_error}
    end.

info(UserIp) ->
    SerialNo = gen_server:call(?SERVER, getsn) ,
    gen_server:cast(?SERVER, {info, [{userip, UserIp},{serialno, SerialNo}]}),
    case waiting_sess_state_change(SerialNo, info, req, ?DEFAULT_TIMEOUT, 0, 10) of
        {NewState, SessAttrs} ->
            case NewState of 
                ack ->
                    gen_server:cast(?SERVER, {remove , [{serialno, SerialNo}]}),
                    {ok, maps:get(attrs,SessAttrs)};
                error ->
                    ErrCode = maps:get(errid,SessAttrs),
                    gen_server:cast(?SERVER, {remove , [{serialno, SerialNo}]}),
                    {error,ErrCode}
            end;
        timeout ->
            gen_server:cast(?SERVER, {remove , [{serialno, SerialNo}]}),
            {error,timeout} ;
        notfound ->
            %lager:warning("Logout Session [~p] is not in session table ~n",[SerialNo]),
            {error,f_error};
        _ ->
            %lager:warning("Logout Session [~p] Type Error ~n",[SerialNo]),
            {error,f_error}
    end.
    

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

waiting_sess_state_change(SerialNo, SessType, StartState, Timeout, WaitingTime, SleepTime) ->
    timer:sleep(SleepTime),
    case gen_server:call(?SERVER, {check, [{serialno, SerialNo}]}) of
        {notfound,_,_} ->
            notfound;
        {SessType, StartState, _} when WaitingTime+SleepTime > Timeout ->
            timeout;
        {SessType, StartState, _}  ->
            waiting_sess_state_change(SerialNo, SessType, StartState, Timeout, WaitingTime+SleepTime,SleepTime*2);
        {SessType, NewState, SessAttrs} ->
            {NewState, SessAttrs};
        _ ->
            typeerr
    end. 
                                

%%%===================================================================
%%% gen_server callbacks
%%%===================================================================

init([BrasIp, Port, Secret]) ->
    {ok, USock} = gen_udp:open(Port, [binary]),
    {ok, #state{bras_ip = BrasIp,secret = Secret,usock = USock,sessions = #{}}, 0}.


handle_call({check,[{serialno, SerialNo}]},_From, State) ->
    {reply,
     maps:get(SerialNo,State#state.sessions,{notfound,req,#{}}),
     State};

handle_call(getsn,_From, State) ->
    rand:seed(exsplus,erlang:timestamp()),
    {reply,
     rand:uniform(16#FFFF),
     State}.
    %% {GS, S, MS} = erlang:timestamp(),
    %% random:seed(GS, S, MS), 
    %% {reply,
    %%  random:uniform(16#FFFF),
    %%  State}.


%%%%
%%% 数据的发送都是异步的
%%%%
handle_cast({auth,[{userip, UserIp},{username, UserName},
                   {password, Password},{serialno,SerialNo}]}, State) ->
    #state{bras_ip=BrasIp ,secret=Secret, usock=USock, sessions=Sessions} = State, 
    gen_udp:send(USock, BrasIp, ?ME60_PORT, 
                 packet_util:make_packet(?REQ_AUTH,SerialNo,pap,0,UserIp,
                                         [{1,UserName},{2,Password}],Secret)),
    %lager:debug("Send auth package: SNo:~p Username:~s Ip:~w ~n",[SerialNo,UserName,UserIp]),
    {noreply, 
     State#state{sessions=maps:put(SerialNo,{login,req,#{}},Sessions)}};
    
handle_cast({aff,[{userip, UserIp},{serialno, SerialNo},{reqid, ReqId}]}, State) ->
    #state{bras_ip=BrasIp ,secret=Secret, usock=USock, sessions=Sessions} = State, 
    gen_udp:send(USock, BrasIp, ?ME60_PORT, 
                 packet_util:make_packet(?AFF_ACK_AUTH,SerialNo,chap,ReqId,UserIp,[],Secret)),
    %lager:debug("Send aff package: SNo:~p Reqid:~p Ip:~w ~n",[SerialNo,ReqId,UserIp]),
    {noreply,
     State#state{sessions=maps:remove(SerialNo,Sessions)} };
    
handle_cast({logout,[{userip, UserIp},{serialno,SerialNo}]}, State) ->
    #state{bras_ip=BrasIp ,secret=Secret, usock=USock, sessions=Sessions} = State, 
    gen_udp:send(USock, BrasIp, ?ME60_PORT, 
                 packet_util:make_packet(?REQ_LOGOUT,SerialNo,chap,0,UserIp,[],Secret)), 
    %lager:debug("Send logout package: SNo:~p Ip:~w ~n",[SerialNo,UserIp]),
    {noreply, 
     State#state{sessions=maps:put(SerialNo,{logout,req,#{}},Sessions)} };

handle_cast({info,[{userip, UserIp},{serialno,SerialNo}]}, State) ->
    #state{bras_ip=BrasIp ,secret=Secret, usock=USock, sessions=Sessions} = State, 
    gen_udp:send(USock, BrasIp, ?ME60_PORT, 
                 packet_util:make_packet(?REQ_INFO,SerialNo,chap,0,UserIp,[],Secret)), 
    %lager:debug("Send logout package: SNo:~p Ip:~w ~n",[SerialNo,UserIp]),
    {noreply, 
     State#state{sessions=maps:put(SerialNo,{info,req,#{}},Sessions)} };

handle_cast({remove,[{serialno, SerialNo}]},State) ->
    Sessions = State#state.sessions,
    {noreply,
     State#state{sessions=maps:remove(SerialNo,Sessions)} };



handle_cast(stop, State) ->
    gen_udp:close(State#state.usock),
    {stop, normal, State};

handle_cast(_Msg, State) ->
    {noreply, State}.

%%%
%% 处理收到的数据报文
%%%%%
handle_info({udp, _Socket, _Ip, ?ME60_PORT, RawData}, State) ->
    case packet_util:unmashalling(RawData) of 
        bypass ->
            {noreply, State};
        {Type,SerialNo,ReqId,_,ErrCode,AttrList} ->
            %lager:debug("Get package: Type:~p SNo:~p ReqId:~p ~n",[Type,SerialNo,ReqId]),
            Sessions = State#state.sessions,
            SessNewValue = 
                case Type of 
                    ?ACK_AUTH when ErrCode == 0 ->
                        {login,ack,#{reqid => ReqId}};
                    ?ACK_AUTH ->
                        {login,error,#{errid => ErrCode}};
                    ?ACK_LOGOUT when ErrCode == 0  ->
                        {logout,ack,#{reqid => ReqId}};
                    ?ACK_LOGOUT ->
                        {logout,error,#{errid => ErrCode}};
                    ?ACK_INFO when ErrCode == 0 ->
                        {info,ack,#{reqid => ReqId, attrs => AttrList}};
                    ?ACK_INFO  ->
                        {info,error,#{errid => ErrCode}};
                    _ ->
                        bypass
                end,
            case maps:is_key(SerialNo,Sessions) of 
                true when SessNewValue /= bypass ->
                    NewSessions = Sessions#{SerialNo := SessNewValue},
                    {noreply, State#state{sessions=NewSessions }} ;
                _ ->
                    %lager:warning("Get package session[~p] is not in session table ~n",[SerialNo]),
                    {noreply, State}
            end
    end;

handle_info(_Msg, State) ->
    {noreply, State}.

code_change(_OldVsn,State,_Extra) ->
    {ok, State}.
    
terminate(_Reason,_State) ->
    ok.

