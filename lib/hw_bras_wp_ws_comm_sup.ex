defmodule HWBrasWebPortalWS.CommSup do
  use Supervisor
    
  # init([]) ->
  #   {ok, BrasIp} = application:get_env(huawei_webportal_ws, bras_ip),
  #   {ok, BrasSecret} = application:get_env(huawei_webportal_ws, bras_secret),

  #   {ok, { {one_for_one, 5, 10}, 
  #          [
  #           {auth_comm, {auth_comm , start_link, [BrasIp,BrasSecret]}, permanent, 5000, worker, [auth_comm]} 
  #          ]} }.

  @name HWBrasWebPortalWS.CommSup

  def start_link() do
    Supervisor.start_link(__MODULE__, :ok, name: @name)
  end

  
  def init(:ok) do
    bras_ip = (System.get_env("BRAS_IP") ||
      Application.get_env(:huawei_bras_webportal_ws, :bras_ip))
      |> to_char_list
    bras_share_key = (System.get_env("BRAS_SHARE_KEY") ||
      Application.get_env(:huawei_bras_webportal_ws, :bras_share_key))
      |> to_char_list

    children = [
      worker(:bras_comm, [bras_ip,bras_share_key])
    ]
    supervise(children, strategy: :one_for_one)
  end

end

