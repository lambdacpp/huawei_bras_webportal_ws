defmodule HWBrasWebPortalWS do
  use Application

  def start(_type, _args) do
    HWBrasWebPortalWS.CommSup.start_link
  end
end
