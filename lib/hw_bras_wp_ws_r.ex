 defmodule HWBrasWebPortalWS.Router.Homepage do
  require Logger
  use Maru.Router
  plug CORSPlug
  version "v1"
  get do
    conn
    |> text("Huawei portal middleware")
  end

  namespace :auth do
    params do
      requires :userip,  type: :string, regexp: ~r/^[0-9.]+$/
      requires :username, type: :string, regexp: ~r/^[\w.@-]+$/
      requires :password, type: :string
    end
    post do
      Logger.debug "Auth request:#{params[:username]} : #{params[:password]} @ #{params[:userip]} ."

      username = params[:username] |> to_char_list
      password = params[:password] |> to_char_list

      case params[:userip] |> to_char_list |> :inet_parse.address do
        {:ok, user_ip} ->
          case :bras_comm.auth(user_ip,username,password) do
            :ok ->
              Logger.info "Auth success:#{params[:username]} @ #{params[:userip]} ."

              conn |> json(%{result: :success}) 
            {:error,errcode} ->
              Logger.debug "Auth fail:#{params[:username]} @ #{params[:userip]},ErrCode=#{errcode} ."
              errinfo = case errcode do
                          1 -> "您的认证请求被拒绝"
                          2 -> "您已经在线"
                          3 -> "您此前的认证尚未结束"
                          4 -> "认证发生错误"
                          _ -> "错误ID=#{errcode}"
                        end
              conn |> json(%{result: :fail,info: "登录失败，#{errinfo}"})
          end
        {:error, _} ->
          conn |> json(%{result: :fail,info: "无效的IP地址"})
      end
    end
  end

  namespace :logout do
    params do
      requires :userip,  type: :string, regexp: ~r/^[0-9.]+$/
    end
    post do
      Logger.debug "Logout request: #{params[:userip]} ."

      case params[:userip] |> to_char_list |> :inet_parse.address do
        {:ok, user_ip} ->
          case :bras_comm.logout(user_ip) do
            :ok ->
              Logger.info "Logout success: #{params[:userip]} ."

              conn |> json(%{result: :success})
            {:error,errcode} ->
              Logger.debug "Logout fail: ErrCode=#{errcode} ."
              errinfo = case errcode do
                          1 -> "您的下线请求被拒绝"
                          2 -> "下线发生错误"
                          _ -> "错误ID=#{errcode}"
                        end
              conn |> json(%{result: :fail,info: "下线错误，#{errinfo}"})
          end

        {:error, _} ->
          conn |> json(%{result: :fail,info: "无效的IP地址"})
      end
    end
  end

  namespace :ip do
    get do
      {{ip1,ip2,ip3,ip4},_port}  =  conn.peer    
      conn |> json(%{ip: "#{ip1}.#{ip2}.#{ip3}.#{ip4}"})
    end
  end

  namespace :status do
    get do
      conn |> json(%{status: :offline}) 
    end
  end
end

defmodule HWBrasWebPortalWS.API do
  use Maru.Router

  before do
    plug Plug.Parsers,
      pass: ["*/*"],
      json_decoder: Poison,
      parsers: [:urlencoded, :json, :multipart]
  end
  
  mount HWBrasWebPortalWS.Router.Homepage

  rescue_from :all do
    conn
    |> put_status(500)
    |> text("Server Error")
  end
end

 
